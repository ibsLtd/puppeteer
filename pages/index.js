import React, {useState, useContext} from 'react'
import Layout from "../components/layout";
import PerformanceProvider, {performanceContext} from "../components/provider";
import Form from '../components/form'
import Scenario from "../components/scenario";
import Results from "../components/results";


const Home = props => {


    return (
        <Layout>

            <PerformanceProvider>
                <section className={'container'}>
                    <div className={'row'}>
                        <h2 className={'col-12'}>{'Choose  scenario'}</h2>
                        <div className={'col-md-6'}>
                            <Scenario/>
                        </div>
                        <div className={'col-md-6'}>
                            <Form/>
                        </div>

                    </div>
                </section>
                <Results/>
            </PerformanceProvider>


        </Layout>


    )

}


export default Home
