const withCSS = require('@zeit/next-css')

module.exports = withCSS({

    webpack: config => {
        // Fixes npm packages that depend on `fs` module
        config.node = {
            fs: 'empty'
        },
            config.plugins.push()
        return config
    }
})
