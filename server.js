const express = require('express')
const next = require('next')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({dev})
const handle = app.getRequestHandler()

const mysql = require('./config/database')
const bodyParser = require("body-parser");
const {runPuppeteer} = require("./puppeteer/default");
const request = require('request');

app.prepare().then(() => {

    const server = express()
    server.use(bodyParser.urlencoded({extended: true}));
    server.use(bodyParser.json());
    server.use("/public", express.static(__dirname + "/public", {
        maxAge: "365d"
    }))

    mysql.getConnection((err, connection) => {
        if (err) console.log(err)
        console.log("Connected!");
    })

    server.get('/', (req, res) => {
        return app.render(req, res, '/index', req.query)
    })

    server.post('/default', async (req, res) => {
        let promises = [],
            results = {}

        const object = {
            //'NUM_BROWSERS': req.body.sessions,
            'homeURL': req.body.domain,
            'testCode': req.body.test
        }

        // for (let i = 0; i < object.NUM_BROWSERS; i++) {
        //     console.log(i, 'request')
        //     promises.push(request(object.homeURL))
        // }
        // await Promise.all(promises)
        // promises.splice(0, promises.length)

        const puppeteer = await runPuppeteer(object.homeURL);

        mysql.query('INSERT INTO results SET test=?,sessions=?,fetchTime=?,domain=?,performance=?,seo=?,accessibility=?,fcp=?,fmp=?,ttfb=?,fci=?,interactive=?,domSize=?,totalPageSize=?',
            [object.testCode, object.NUM_BROWSERS, puppeteer.fetchTime, object.homeURL, puppeteer.performance, puppeteer.seo, puppeteer.accessibility, puppeteer.fcp.numericValue, puppeteer.fmp.numericValue, puppeteer.ttfb.numericValue, puppeteer.fci.numericValue, puppeteer.interactive.numericValue, puppeteer.domSize.numericValue, puppeteer.totalPageSize],
            (error, results, fields) => {
                if (error) console.log(error)
            }
        );
        await res.send({...puppeteer})
    })

    server.all('*', (req, res) => {
        return handle(req, res)
    })

    server.listen(port, err => {
        if (err) throw err
        console.log(`> Ready on http://localhost:${port}`)
    })
})
