import React, {useEffect} from "react";
import NextHead from 'next/head'
import Footer from "./footer";
import Header from "./header";


const Layout = props => {


    return (
        <React.Fragment>

            <NextHead>
                <meta charSet="UTF-8"/>
                <title>{props.title || ''}</title>
                <meta name="description" content={props.description || ''}/>
                <meta name="keywords" content={props.keywords || ''}/>
                <meta name='viewport' content='initial-scale=1.0, width=device-width'/>
                <meta name="theme-color" content="#fff"/>

                <link rel="preconnect dns-prefetch" href="https://fonts.gstatic.com" crossOrigin={'true'}/>
                <link rel="dns-prefetch" href="//cdnjs.cloudflare.com" crossOrigin={'true'}/>
                <link rel="dns-prefetch" href="//ajax.googleapis.com" crossOrigin={'true'}/>
                <link rel="dns-prefetch" href="//s.w.org" crossOrigin={'true'}/>

                <link
                    href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
                    rel="stylesheet"/>
                <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap-grid.css"
                      rel="stylesheet"/>
                <link href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
                      rel="stylesheet"/>

                <link type="text/css" rel="stylesheet" href={"/public/modules.css"}/>
                <link type="text/css" rel="stylesheet" href={"/public/style.css"}/>

            </NextHead>

            <Header/>
            {props.children}
            {/*<Footer/>*/}
        </React.Fragment>
    )
}
export default Layout
