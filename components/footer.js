import React from 'react'


const Footer = props => {


    return (
        <footer>
            <div className={'container'}>
                <div className={'row'}>
                    <div className={'col-md-6'}>
                        <h4>{'Copyright (c) 2019 iBS Ltd, All rights reserved'}</h4>

                    </div>
                    <div className={'col-md-6'}>
                        <div className={'social-icons'}>
                            <a href={'https://www.linkedin.com/company/ibs-piraeus'} rel={"noopener"} target={"_blank"}><i className="fa fa-linkedin"/></a>
                            <a href={'https://www.facebook.com/ibs.gr/?ref=br_rs'} rel={"noopener"} target={"_blank"}><i className="fa fa-facebook-f"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer
