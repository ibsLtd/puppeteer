import React from 'react'
import Link from 'next/link'


const Header = props => {


    return (
        <header>
            <div className={'container'}>

                <Link href={'/'} as={`/`}>
                    <a><img src={`/assets/logo.png`} alt={'Novatech Solutions'}/></a>
                </Link>


            </div>
        </header>
    )
}

export default Header
