import React, {useState, useContext} from 'react'
import {performanceContext} from './provider'

const Form = props => {

    const [domain, setDomain] = useState('')
    const [company, setCompany] = useState('')
    const contextValue = useContext(performanceContext);


    const handleSubmit = async e => {
        e.preventDefault()

        await fetch('/default', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            json: true,
            body: JSON.stringify({domain: domain, test: company})
        })
            .then(res => res.json())
            .then(res => {
                setDomain('')
                setCompany('')
                contextValue.getResponse({...res})

            })
            .catch(error => console.log(error));

    }

    return (
        <form className={'d-flex flex-column'}>

            <input type="text" id="domain" placeholder={'Domain'}
                   onChange={e => setDomain(e.target.value)}
                   value={domain}/>
            <input type="text" id="testCode" placeholder={'Test Name'}
                   onChange={e => setCompany(e.target.value)}
                   value={company}/>
            {/*<input type="text" id="sessions" placeholder={'Sessions'}*/}
            {/*       onChange={e => setSessions(e.target.value)} value={sessions}/>*/}
            <button type={'Submit'} onClick={(e) => handleSubmit(e)}>{'Submit'}</button>


            </form>
    )
}

export default Form
