import React, {useState} from 'react'

export const performanceContext = React.createContext();

const PerformanceProvider = (props) => {


    const [scenario, setScenario] = useState('default');
    const [json, setJson] = useState({});

    const _setScenario = (item) => {
        setScenario(item)
    }

    const resJson = (items) => {
      setJson(items)
    }


    return (
        <performanceContext.Provider
            value={{scenario: scenario, res: json, getResponse: resJson, setScenario: _setScenario}}>

            {props.children}
        </performanceContext.Provider>
    );
};

export default PerformanceProvider



