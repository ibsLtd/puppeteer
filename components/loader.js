import React from 'react'


const Loader = props => {


    return (
        <div className="loader" style={props.loader ? {display: 'block'} : {display: 'none'}}/>
    )
}

export default Loader
