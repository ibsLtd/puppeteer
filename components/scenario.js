import React, {useState, useContext} from 'react'
import {performanceContext} from './provider'


const Scenario = props => {

    const [checked, setChecked] = useState('default')
    const contextValue = useContext(performanceContext);


    const handleScenario = (type, e) => {
        setChecked(e.currentTarget.value)
        contextValue.setScenario(e.currentTarget.value)
    }


    return (


        <div>
            <input type="radio" id="default" value={'default'} checked={checked === "default"}
                   onChange={(e) => handleScenario('default', e)}/>
            <label htmlFor="default">{'Pagespeed test'}</label>
        </div>
    )
}

export default Scenario
