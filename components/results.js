import React, {useContext} from 'react'
import {performanceContext} from './provider'

const Results = props => {

    const contextValue = useContext(performanceContext);
    const json = contextValue.res

    return (
        <section className={'container results'}>
            {Object.entries(json).length !== 0 &&
            <>
                <div className={'row'}>
                    <div className={'col-12 col-md-4 '}>
                        <div className={'d-flex justify-content-between align-items-center box'}>
                            <p>{'Performance'}</p>
                            <div className={'pos-rel d-flex justify-content-end  score '}>
                                <svg viewBox="0 0 36 36" className="graph">
                                    <path className="circle"
                                          strokeDasharray={json.performance}
                                          d="M18 2.0845
                                   a 15.9155 15.9155 0 0 1 0 31.831
                                   a 15.9155 15.9155 0 0 1 0 -31.831"
                                    />
                                </svg>
                                <p className={'pos-abs center'}>{json.performance}</p>
                            </div>
                        </div>

                    </div>
                    <div className={'col-12 col-md-4 '}>
                        <div className={'d-flex justify-content-between align-items-center box'}>
                            <p>{'Seo'}</p>
                            <div className={'pos-rel d-flex justify-content-end score'}>
                                <svg viewBox="0 0 36 36" className="graph">
                                    <path className="circle"
                                          strokeDasharray={json.seo}
                                          d="M18 2.0845
                                   a 15.9155 15.9155 0 0 1 0 31.831
                                   a 15.9155 15.9155 0 0 1 0 -31.831"
                                    />
                                </svg>
                                <p className={'pos-abs center'}>{json.seo}</p>
                            </div>
                        </div>
                    </div>
                    <div className={'col-12 col-md-4 '}>
                        <div className={'d-flex justify-content-between  align-items-center box'}>
                            <p>{'Accessibility'}</p>
                            <div className={'pos-rel d-flex justify-content-end score'}>
                                <svg viewBox="0 0 36 36" className="graph">
                                    <path className="circle"
                                          strokeDasharray={json.accessibility}
                                          d="M18 2.0845
                                   a 15.9155 15.9155 0 0 1 0 31.831
                                   a 15.9155 15.9155 0 0 1 0 -31.831"
                                    />
                                </svg>
                                <p className={'pos-abs center'}>{json.accessibility}</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div className={'row'}>
                    <div className={' col-6 col-md-3 '}>
                        <div className={'box'}>
                            <p>{json.fcp.title}</p>
                            <p dangerouslySetInnerHTML={{__html: json.fcp.description}}/>
                            <p>{json.fcp.numericValue}</p>
                        </div>

                    </div>
                    <div className={'col-6 col-md-3 '}>
                        <div className={'box'}>

                            <p>{json.fmp.title}</p>
                            <p dangerouslySetInnerHTML={{__html: json.fmp.description}}/>
                            <p>{json.fmp.numericValue}</p>
                        </div>

                    </div>
                    <div className={'col-6 col-md-3 '}>
                        <div className={'box'}>
                            <p>{json.ttfb.title}</p>
                            <p dangerouslySetInnerHTML={{__html: json.ttfb.description}}/>
                            <p>{json.ttfb.numericValue}</p>
                        </div>


                    </div>
                    <div className={' col-6 col-md-3 '}>
                        <div className={'box'}>

                            <p>{json.fci.title}</p>
                            <p dangerouslySetInnerHTML={{__html: json.fci.description}}/>
                            <p>{json.fci.numericValue}</p>
                        </div>

                    </div>
                    <div className={'col-6 col-md-3 '}>
                        <div className={'box'}>
                            <p>{json.interactive.title}</p>
                            <p dangerouslySetInnerHTML={{__html: json.interactive.description}}/>
                            <p>{json.interactive.numericValue}</p>
                        </div>


                    </div>
                    <div className={' col-6 col-md-3'}>
                        <div className={'box'}>
                            <p>{json.domSize.title}</p>
                            <p>{`${json.domSize.numericValue} elements`} </p>
                        </div>

                    </div>
                    <div className={' col-6 col-md-3'}>
                        <div className={'box'}>
                            <p>{'Total Page Size'}</p>
                            <p>{`${json.totalPageSize} kb`}</p>
                        </div>

                    </div>
                </div>

            </>
            }
        </section>
    )
}

export default Results
