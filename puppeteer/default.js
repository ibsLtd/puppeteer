"use strict"
const puppeteer = require('puppeteer');
const chromeLauncher = require('chrome-launcher');

const lighthouse = require('lighthouse');
const config = require('lighthouse/lighthouse-core/config/lr-desktop-config.js');
const reportGenerator = require('lighthouse/lighthouse-core/report/report-generator');

const util = require('util');
const request = require('request');

const runPuppeteer = async (homeURL) => {

    const opts = {
        headless: false,
        logLevel: 'error',
        output: 'json',
        disableDeviceEmulation: true,
        defaultViewport: {
            width: 1200,
            height: 900
        },
        args: [
            '--disable-gpu',
            '--disable-dev-shm-usage',
            '--disable-setuid-sandbox',
            '--no-first-run',
            '--no-sandbox',
            '--no-zygote',
            '--single-process', // <- this one doesn't works in Windows
        ]
        //chromeFlags: [ '--disable-mobile-emulation', '--no-sandbox', '--disable-setuid-sandbox'],
    };


    // Launch chrome using chrome-launcher
    const chrome = await chromeLauncher.launch(opts);
    opts.port = chrome.port;


    // Connect to it using puppeteer.connect().
    const resp = await util.promisify(request)(`http://localhost:${opts.port}/json/version`);
    const {webSocketDebuggerUrl} = JSON.parse(resp.body);
    const browser = await puppeteer.connect({browserWSEndpoint: webSocketDebuggerUrl});

    const page = (await browser.pages())[0];
    await page.goto(homeURL, {waitUntil: 'networkidle2'});
    const results = await runLighthouseForURL(homeURL, opts);

    await browser.disconnect();
    await chrome.kill();

    console.log('puppeteer done')
    return {...results}

}

const runLighthouseForURL = async (pageURL, opts) => {
    console.log('run')
    const report = await lighthouse(pageURL, opts, config).then(results => {
        return results;
    });
    const json = reportGenerator.generateReport(report.lhr, 'json');
    const res = JSON.parse(json)

    const object = {
        performance: res.categories.performance.score * 100,
        accessibility: res.categories.accessibility.score * 100,
        seo: res.categories.seo.score * 100,
        fetchTime: res.fetchTime,
        requestedUrl: res.requestedUrl,
        fcp: {
            title: res.audits['first-contentful-paint'].title,
            description: "<p>First Contentful Paint marks the time at which the first text or image is painted.</p><a href='https://web.dev/first-contentful-paint' target='_blank'>Learn more</a>",
            numericValue: res.audits['first-contentful-paint'].displayValue
        },
        fmp: {
            title: res.audits['first-meaningful-paint'].title,
            description: "<p>First Meaningful Paint measures when the primary content of a page is visible.</p> <a href='https://web.dev/first-meaningful-paint' target='_blank'>Learn more</a>",
            numericValue: res.audits['first-meaningful-paint'].displayValue
        },
        ttfb: {
            title: res.audits['time-to-first-byte'].title,
            description: "<p>Time To First Byte identifies the time at which your server sends a response. </p><a href='https://web.dev/time-to-first-byte' target='_blank'>Learn more</a>",
            numericValue: res.audits['time-to-first-byte'].displayValue
        },
        fci: {
            title: res.audits['first-cpu-idle'].title,
            description: "<p>First CPU Idle marks the first time at which the page 's main thread is quiet enough to handle input.</p> <a href='https://web.dev/first-cpu-idle' target='_blank'>Learn more</a>",
            numericValue: res.audits['first-cpu-idle'].displayValue

        },
        interactive: {
            title: res.audits.interactive.title,
            description: "<p>Time to interactive is the amount of time it takes for the page to become fully interactive.</p><a href='https://web.dev/interactive' target='_blank'>Learn more</a>",
            numericValue: res.audits.interactive.displayValue
        },
        domSize: {
            title: res.audits['dom-size'].title,
            numericValue: res.audits['dom-size'].displayValue
        },
        totalPageSize: 10,
        //totalPageSize: res.audits['network-requests'].details.items.find(request => request.mimeType === 'text/html' || request.resourceType === 'document').transferSize
    }


    return {...object}
}


exports.runPuppeteer = runPuppeteer




